<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
          integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ=="
          crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"
            integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ=="
            crossorigin="anonymous"></script>
    <title>${title}</title>

</head>
<body>

<div class="container">
    <div class="row">
        <h1 class="text-center">${appName}</h1>
    </div>
    <div class="row">
        <p>Do you authorize '${appName}' to access your protected resources:</p>
    </div>
    <div class="row">
        <table class="table" data-show-header="false">
            <thead>
            <tr>
                <th data-field="title">Authorization title</th>
                <th data-field="description">Description</th>
            </tr>
            </thead>
            <tbody>
            <#list authorizedGant as types>
            <tr>
                <td>${types.title}</td>
                <td>${types.description}</td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>

    <div class="row">
        <form class="form-inline text-right" id="confirmationForm" name="confirmationForm"
              action="<@spring.url '/oauth/authorize'/>" method="post">
            <input name="user_oauth_approval" value="true" type="hidden"/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input id="submit-access" name="scope.all" value="" type="hidden"/>
            <button id="submit-deny" name="deny" type="submit" class="btn btn-default">Deny</button>
            <button id="submit-approve" name="authorize" value="Authorize" type="submit" class="btn btn-primary">Authorize</button>
            <script type="text/javascript">
                        $('#submit-approve').click(function(){$('#submit-access').val(true);});
            </script>
        </form>
    </div>
</div>
</body>
</html>
