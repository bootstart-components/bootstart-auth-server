package tech.bootstart.auth.server.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import tech.bootstart.auth.server.domain.Account;
//import tech.bootstart.auth.server.repository.UserRepository;
import java.util.HashSet;
import java.util.Set;


/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

    @Autowired
    private AccountRepository accountRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);
        String lowercaseLogin = login.toLowerCase();
        Account accountFromDatabase = accountRepository.findOneByEmail(lowercaseLogin);

        if(accountFromDatabase.getId() != null && login.equalsIgnoreCase(accountFromDatabase.getEmail())) {
            if(!accountFromDatabase.getActivated()) {
                throw new UserNotActivatedException("Account " + lowercaseLogin + " was not activated");
            }
            Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
            authorities.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "ROLE_USER";
                }
            });
            CustomUserDetails userDetails = new CustomUserDetails(accountFromDatabase.getId(), lowercaseLogin,
                    accountFromDatabase.getPassword(), true, true, true, true, authorities);
            return userDetails;
        }
        throw new UsernameNotFoundException("Account " + lowercaseLogin + " was not found in the " +
        "database");
    }
}
