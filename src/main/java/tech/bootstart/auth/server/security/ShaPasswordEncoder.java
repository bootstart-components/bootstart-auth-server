package tech.bootstart.auth.server.security;

import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

/**
 * Created by florian on 29/10/15.
 */
public class ShaPasswordEncoder implements PasswordEncoder {
        private final Logger log = LoggerFactory.getLogger(AccountRepository.class);

    @Override
    public String encode(CharSequence rawPassword) {
        ByteBuffer bPassword = Charsets.UTF_8.encode(CharBuffer.wrap(rawPassword));
        return DigestUtils.sha1Hex(bPassword.array());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encode(rawPassword).equals(encodedPassword);
    }
}
