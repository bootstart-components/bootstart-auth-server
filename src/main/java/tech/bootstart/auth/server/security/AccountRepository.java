package tech.bootstart.auth.server.security;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import tech.bootstart.auth.server.domain.Account;

import javax.inject.Inject;
import java.io.IOException;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

/**
 * Created by florian on 29/10/15.
 */
@Component("ClientRepository")
public class AccountRepository {
    private final Logger log = LoggerFactory.getLogger(AccountRepository.class);

    @Inject
    Environment env;
    @Autowired
    Client client;

    public static ObjectMapper mapper = new ObjectMapper();


    public Account findOneByEmail(String email) {

        BoolQueryBuilder queryB = QueryBuilders.boolQuery();

        queryB.must(termQuery("emails.value", email));
        SearchRequestBuilder search = client.prepareSearch(env.getProperty("elasticsearch.index.name"))
                .setTypes(env.getProperty("elasticsearch.index.type"))
                .setQuery(queryB)
                .setFrom(0).setSize(1);
        SearchResponse response = search.execute().actionGet();
        Account account = new Account();
        try {
            JsonNode responseNode = mapper.readTree(response.toString());
            responseNode = responseNode.findPath("hits");
            log.info(responseNode.toString());
            if(responseNode.findValue("total").asInt() == 1) {
                JsonNode userNode = responseNode.findPath("hits");
                account.setId(userNode.findValue("_id").asText());
                userNode = userNode.findPath("_source");
                for (JsonNode jsonEmail : userNode.findValue("emails")) {

                    if (jsonEmail.findValue("value").asText().equalsIgnoreCase(email)) {
                        account.setEmail(email);
                        account.setActivated(userNode.findValue("status").asText().equalsIgnoreCase("validated"));
                    }
                }
                account.setPassword(userNode.findValue("password").findValue("value").asText());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return account;
    }
}
