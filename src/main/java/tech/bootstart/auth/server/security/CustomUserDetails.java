package tech.bootstart.auth.server.security;

import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Class representing the Spring security authenticated user.
 *
 * @see UserDetails
 *
 */
public class CustomUserDetails extends User {

    private static final long serialVersionUID = 1L;

    private final String id;

    /**
     * @param id
     * @param username
     * @param password
     * @param accountNonExpired
     * @param accountNonLocked
     * @param credentialsNonExpired
     * @param enabled
     */
    public CustomUserDetails(String id, String username, String password, boolean accountNonExpired, boolean accountNonLocked,
                             boolean credentialsNonExpired, boolean enabled, Set<GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "CustomUserDetails{" + id + ',' + getUsername() + ',' /*+ authorities*/ + '}';
    }
}

