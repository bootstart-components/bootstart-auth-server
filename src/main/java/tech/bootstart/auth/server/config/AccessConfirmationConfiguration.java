package tech.bootstart.auth.server.config;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import tech.bootstart.auth.server.domain.ApprovalParameters;

import javax.inject.Inject;

/**
 * Created by florian on 02/11/15.
 */
@Controller
@SessionAttributes("authorizationRequest")
public class AccessConfirmationConfiguration {

    @Autowired
    private ApprovalParameters approvalParameters;

    @Inject
    private Environment env;

    @RequestMapping("/oauth/confirm_access")
    public ModelAndView getAccessConfirmation(Map<String, Object> model) throws Exception {
	    AuthorizationRequest clientAuth = (AuthorizationRequest) model.remove("authorizationRequest");
        model.put("auth_request", clientAuth);
        ModelAndView mav = new ModelAndView("access_confirmation", model);
        mav.addObject("authorizedGant", approvalParameters.getTypes());
        mav.addObject( "appName", env.getProperty("application.name"));
        mav.addObject("title", env.getProperty("application.title"));
        return mav;
    }

    @RequestMapping("/oauth/error")
    public String handleError(Map<String, Object> model) throws Exception {
        // We can add more stuff to the model here for JSP rendering. If the client was a machine then
        // the JSON will already have been rendered.
        model.put("message", "There was a problem with the OAuth2 protocol");
        return "oauth_error";
    }
}
