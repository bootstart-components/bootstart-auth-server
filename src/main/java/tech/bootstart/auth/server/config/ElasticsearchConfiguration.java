package tech.bootstart.auth.server.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

@Configuration
public class ElasticsearchConfiguration {
    @Resource
    private Environment environment;
    @Bean
    public Client client() {
        Settings elasticsearchSettings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", environment.getProperty("elasticsearch.cluster.name"))
                .put("client.transport.sniff", Boolean.parseBoolean(environment.getProperty("elasticsearch.cluster.sniff")))
                .build();
        TransportClient client = new TransportClient(elasticsearchSettings);
        TransportAddress address = new InetSocketTransportAddress(environment.getProperty("elasticsearch.cluster.address")
                , Integer.parseInt(environment.getProperty("elasticsearch.cluster.port")));
        client.addTransportAddress(address);
        return client;
    }

}