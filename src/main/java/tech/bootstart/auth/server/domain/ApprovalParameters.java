package tech.bootstart.auth.server.domain;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by florian on 02/11/15.
 */
@Configuration
@ConfigurationProperties(prefix = "authorized-grant")
public class ApprovalParameters {

    private List<Type> types;

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    public String[] keyArrayString() {

        List<String> ret = types.stream().map(Type::getKey).collect(Collectors.toList());
        String a[] = new String[ret.size()];
        ret.toArray(a);
        return a;
    }

    public static class Type {
        private String key;
        private String title;
        private String description;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }


}
